package main

import (
	"log"
	"time"

	"gitlab.com/danielzierl/sensor-reading-go-aws-aimtec-reporter/internal"
	"gitlab.com/danielzierl/sensor-reading-shared-libs/logging"
	"gitlab.com/danielzierl/sensor-reading-shared-libs/models"
)

func main() {
	logging.SetupLoggingAuto("aimtec-reporter")
	defer logging.CloseLogFile()

	// Test Requests
	internal.LoginUntilSuccess()
	internal.RunReporting()
	// testRequests()
}

func testRequests() {
	log.Println()
	internal.ReadAllSensors()
	sensorUUIDS := []string{"6fba87a7-28d9-4769-988a-3af3cd1422b8", "b8184e94-28e9-4785-a846-8de6fe171717"}
	internal.ReadAllMeasurements(sensorUUIDS)
	internal.ReadAllAlerts(sensorUUIDS)

	measurement := models.MeasurementBody{
		CreatedOn:   internal.TimeToRFCFormat(time.Now()),
		SensorUUID:  "7dcc466e-b2d7-408d-b23e-8f21f6104c2a",
		Temperature: 1.0,
		Status:      "TEST",
	}

	outcome := internal.CreateMeasurement(measurement)
	log.Printf("Measurement outcome: %v\n", outcome)

	alert := models.AlertBody{
		CreatedOn:       internal.TimeToRFCFormat(time.Now()),
		SensorUUID:      "d8d26463-ad60-411f-804b-0cf34a2560c5",
		Temperature:     1.0,
		LowTemperature:  1.0,
		HighTemperature: 1.0,
	}
	internal.CreateAlert(alert)
	log.Printf("Alert outcome: %v\n", outcome)
}
