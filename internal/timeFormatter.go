package internal

import (
	"fmt"
	"time"
)

func TimeToRFCFormat(in_time time.Time) string {
	out_time := in_time.Format(time.RFC3339)
	out_time = fmt.Sprintf("%s.%03d%s", out_time[:19], 0, out_time[19:])

	return out_time
}
