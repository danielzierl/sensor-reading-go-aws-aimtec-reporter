package internal

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"time"

	"gitlab.com/danielzierl/sensor-reading-shared-libs/models"
)

const SLEEP_TIME_SEC = 10

var team_uuid = ""

func LoginUntilSuccess() {
	for !Login() {
		log.Println("Login failed, retrying...")
		time.Sleep(SLEEP_TIME_SEC * time.Second)
	}
}

func Login() bool {
	login_info := []byte(fmt.Sprintf(`{"username": "%s", "password": "%s"}`, os.Getenv("AIMTEC_AWS_USERNAME"), os.Getenv("AIMTEC_AWS_PASS")))
	request, _ := http.NewRequest("POST", fmt.Sprintf("%s/login", os.Getenv("AIMTEC_AWS_SERVER")), bytes.NewBuffer(login_info))
	request.Header.Set("Content-Type", "application/json")

	response, _ := sendRequest(request)

	log.Println("Response Status:", response.StatusCode)
	log.Println("Response Headers:", response.Header)

	data := convertResponseToStruct(response)
	log.Println("Data:", data)
	uuid, ok := data["teamUUID"].(string)
	if ok {
		team_uuid = uuid
		log.Println("team_uuid:", team_uuid)
		return true
	}
	return false
}

func convertResponseToStruct(response *http.Response) map[string]interface{} {
	var data map[string]interface{}
	err := json.NewDecoder(response.Body).Decode(&data)
	if err != nil {
		log.Panicln("Error parsing JSON:", err)
		return nil
	}
	defer response.Body.Close()
	return data
}

func sendRequest(request *http.Request) (*http.Response, error) {
	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		return nil, err
	}
	return response, nil
}

func ReadAllSensors() []string {
	if team_uuid == "" {
		LoginUntilSuccess()
	}

	request, _ := http.NewRequest("GET", fmt.Sprintf("%s/sensors", os.Getenv("AIMTEC_AWS_SERVER")), nil)
	request.Header.Set("teamUUID", team_uuid)

	response, _ := sendRequest(request)

	body, _ := io.ReadAll(response.Body)

	var sensors []models.Sensor
	err := json.Unmarshal([]byte(body), &sensors)
	if err != nil {
		log.Panicln("Error parsing JSON:", err)
		return nil
	}

	sensor_uuids := make([]string, len(sensors))

	for _, sensor := range sensors {
		sensor_uuid := sensor.SensorUUID
		log.Println("sensor_uuid:", sensor_uuid)
		sensor_uuids = append(sensor_uuids, sensor_uuid)
	}

	return nil

}

func CreateMeasurement(measurement models.MeasurementBody) bool {

	measurementJSON, err := json.Marshal(measurement)
	if err != nil {
		return false
	}

	request, _ := http.NewRequest("POST", fmt.Sprintf("%s/measurements", os.Getenv("AIMTEC_AWS_SERVER")), bytes.NewBuffer(measurementJSON))
	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("teamUUID", team_uuid)

	response, err := sendRequest(request)
	if err != nil {
		return false
	}
	fmt.Println(response)
	return true
}

func CreateAlert(alert models.AlertBody) bool {

	alertJSON, err := json.Marshal(alert)
	if err != nil {
		return false
	}

	request, _ := http.NewRequest("POST", fmt.Sprintf("%s/alerts", os.Getenv("AIMTEC_AWS_SERVER")), bytes.NewBuffer(alertJSON))
	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("teamUUID", team_uuid)

	response, err := sendRequest(request)
	if err != nil {
		return false
	}
	fmt.Println(response)
	return true
}

func ReadAllMeasurements(sensorUUIDs []string) []models.MeasurementBody {
	request, _ := http.NewRequest("GET", fmt.Sprintf("%s/measurements", os.Getenv("AIMTEC_AWS_SERVER")), nil)
	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("teamUUID", team_uuid)

	response, err := sendRequest(request)
	if err != nil {
		log.Println("ReadAllMeasurements Response Error:", err)
		return nil
	}

	body, err := io.ReadAll(response.Body)
	if err != nil {
		log.Println("ReadAllMeasurements Reading Body Error:", err)
		return nil
	}

	var measurements []models.MeasurementBody
	err = json.Unmarshal([]byte(body), &measurements)
	if err != nil {
		log.Println("Error parsing JSON:", err)
		return nil
	}
	return measurements
}

func ReadAllAlerts(sensorUUIDs []string) []models.AlertBody {
	request, _ := http.NewRequest("GET", fmt.Sprintf("%s/alerts", os.Getenv("AIMTEC_AWS_SERVER")), nil)
	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("teamUUID", team_uuid)

	response, err := sendRequest(request)
	if err != nil {
		log.Println("ReadAllAlerts Response Error:", err)
		return nil
	}

	body, err := io.ReadAll(response.Body)
	if err != nil {
		log.Println("ReadAllAlerts Reading Body Error:", err)
		return nil
	}

	var alerts []models.AlertBody
	err = json.Unmarshal([]byte(body), &alerts)
	if err != nil {
		log.Println("Error parsing JSON:", err)
		return nil
	}
	return alerts
}
