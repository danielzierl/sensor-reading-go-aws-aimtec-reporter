package internal

import (
	"errors"
	"fmt"
	"log"
	"os"
	"time"

	"gitlab.com/danielzierl/sensor-reading-shared-libs/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

const UPDATE_INTERVAL = 60
const SEND_ONLY_ONE = true

var db *gorm.DB

func SetupAndConnectDb() {
	var DB_PATH = os.Getenv("DB_LOCATION")
	_db, err := gorm.Open(postgres.Open(DB_PATH), &gorm.Config{})
	if err != nil {
		log.Panic("Failed to connect to the main database")
	}
	db = _db
}

// Get specific team's sensors
const temperature_sensor_name = "sensor11_temperature"
const humidity_sensor_name = "sensor12_humidity"
const illumination_sensor_name = "sensor13_illumination"

var temperature_sensor models.Sensor
var humidity_sensor models.Sensor
var illumination_sensor models.Sensor

func SetupSensors() {
	db.Where("name = ?", humidity_sensor_name).First(&humidity_sensor)
	db.Where("name = ?", illumination_sensor_name).First(&illumination_sensor)
	db.Where("name = ?", temperature_sensor_name).First(&temperature_sensor)
}

func RunReporting() {
	SetupAndConnectDb()
	SetupSensors()
	for {
		if os.Getenv("REPORT_MODE") != "TEST" {
			ReportAlerts()
		}
		ReportMeasurements()
		time.Sleep(UPDATE_INTERVAL * time.Second)
	}
}

func ReportAlerts() {
	alerts, err := getAlertsFromDB() // Get TEAM_NAME alerts
	if err != nil {
		fmt.Println("No new alerts found in DB:", err)
		return
	}
	fmt.Printf("Found %d alerts\n", len(*alerts))
	for _, alert := range *alerts {
		log.Printf("Reporting alert: %+v\n", alert)
		alert_body := transferAlertToAlertBody(&alert)
		CreateAlert(*alert_body)
	}

}

var lastAlertId *uint = nil

func getAlertsFromDB() (*[]models.Alert, error) {
	var alerts []models.Alert
	if lastAlertId == nil {
		db.Preload("Sensor").Last(&alerts)
		if len(alerts) == 0 {
			return nil, errors.New("No alerts found")
		}
		lastAlertId = &alerts[0].ID
	} else {
		db.Preload("Sensor").Where("ID > ?", *lastAlertId).Find(&alerts)

		if len(alerts) == 0 {
			return nil, errors.New("No alerts found")
		}
		lastAlertId = &alerts[len(alerts)-1].ID
	}
	return &alerts, nil
}

func transferAlertToAlertBody(alert *models.Alert) *models.AlertBody {
	return &models.AlertBody{
		CreatedOn:       TimeToRFCFormat(alert.CreatedAt),
		SensorUUID:      alert.Sensor.SensorUUID,
		Temperature:     alert.MeasuredValue,
		LowTemperature:  alert.Sensor.MinTemperature,
		HighTemperature: alert.Sensor.MaxTemperature,
	}
}

func ReportMeasurements() {
	sensorReadings, err := getSensorReadingsFromDB() // Get TEAM_NAME readings
	if err != nil {
		log.Println("No new measurements found in DB:", err)
		return
	}

	fmt.Printf("Found %d sensor readings\n", len(*sensorReadings))
	for _, reading := range *sensorReadings {
		fmt.Printf("Reporting sensor reading: %+v\n", reading)
		measurements := transferSensorReadingToMeasurements(&reading)
		for _, measurement := range *measurements {
			fmt.Printf("Creating measurement, sending to AWS: %+v\n", measurement)
			CreateMeasurement(measurement)
		}

	}
}

var lastSensorReadingId *uint = nil

func getSensorReadingsFromDB() (*[]models.SensorReading, error) {
	TEAM_NAME := os.Getenv("TEAM_NAME")
	var sensorReadings []models.SensorReading
	if lastSensorReadingId == nil {
		db.Where("team_name", TEAM_NAME).Last(&sensorReadings)
		if len(sensorReadings) == 0 {
			return nil, errors.New("No sensor readings found")
		}
		lastSensorReadingId = &sensorReadings[0].ID
	} else {
		if SEND_ONLY_ONE {
			db.Where("team_name", TEAM_NAME).Where("ID > ?", *lastSensorReadingId).Last(&sensorReadings)
		} else {
			db.Where("team_name", TEAM_NAME).Where("ID > ?", *lastSensorReadingId).Find(&sensorReadings)
		}

		if len(sensorReadings) == 0 {
			return nil, errors.New("No sensor readings found")
		}
		lastSensorReadingId = &sensorReadings[len(sensorReadings)-1].ID
	}
	return &sensorReadings, nil
}

func transferSensorReadingToMeasurements(sensorReading *models.SensorReading) *[]models.MeasurementBody {
	REPORT_MODE := os.Getenv("REPORT_MODE") // TEST / OK
	var measurements []models.MeasurementBody

	// Make Temperature Measurement
	if sensorReading.Temperature != 0 {
		measurement := models.MeasurementBody{
			CreatedOn:   TimeToRFCFormat(sensorReading.CreatedAt),
			SensorUUID:  temperature_sensor.SensorUUID,
			Temperature: sensorReading.Temperature,
			Status:      REPORT_MODE,
		}
		measurements = append(measurements, measurement)
	}
	// Make Humidity Measurement
	if sensorReading.Humidity != 0 {
		measurement := models.MeasurementBody{
			CreatedOn:   TimeToRFCFormat(sensorReading.CreatedAt),
			SensorUUID:  humidity_sensor.SensorUUID,
			Temperature: sensorReading.Humidity, // Humidity is temperature in this case (AIMTEC)
			Status:      REPORT_MODE,
		}
		measurements = append(measurements, measurement)
	}
	// Make Illumination Measurement
	if sensorReading.Illumination != 0 {
		measurement := models.MeasurementBody{
			CreatedOn:   TimeToRFCFormat(sensorReading.CreatedAt),
			SensorUUID:  illumination_sensor.SensorUUID,
			Temperature: sensorReading.Illumination,
			Status:      REPORT_MODE,
		}
		measurements = append(measurements, measurement)
	}
	return &measurements
}
